<?php

namespace Academy\Book\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Data {
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    const XML_PATH_IS_ENABLED = "academy/book/enabled";

    const XML_PATH_BOOKS_PER_PAGE = "academy/book/books_per_page";


    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_IS_ENABLED);
    }


    public function getBooksPerPage()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_BOOKS_PER_PAGE);
    }

}