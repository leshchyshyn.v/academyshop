<?php

namespace Academy\Book\Block\Adminhtml\Book\Edit;

use Magento\Backend\Block\Widget\Context;
use Academy\Book\Api\BookRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var BookRepositoryInterface
     */
    protected $bookRepository;

    /**
     * @param Context $context
     * @param BookRepositoryInterface $bookRepository
     */
    public function __construct(
        Context $context,
        BookRepositoryInterface $bookRepository
    ) {
        $this->context = $context;
        $this->bookRepository = $bookRepository;
    }

    /**
     * Return CMS block ID
     *
     * @return int|null
     */
    public function getBookId()
    {
        try {
            return $this->bookRepository->getById(
                $this->context->getRequest()->getParam('book_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
