<?php

namespace Academy\Book\Controller\Adminhtml\Book;

use Academy\Book\Api\BookRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Backend\App\Action;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Book::book';

    /**
     * @var BookRepositoryInterface
     */
    private $bookRepository;

    /**
     * MassDelete constructor.
     * @param Action\Context $context
     * @param BookRepositoryInterface $bookRepository
     */
    public function __construct(
        Action\Context $context,
        BookRepositoryInterface $bookRepository
    )
    {
        parent::__construct($context);
        $this->bookRepository = $bookRepository;
    }

    public function execute()
    {
        $selectedIds = $this->getRequest()->getParam("selected");
        foreach ($selectedIds as $book)
        {
            try {
                $this->bookRepository->deleteById($book);
            } catch (CouldNotDeleteException $ex) {}
        }
        $redirect = $this->resultRedirectFactory->create();
        return $redirect->setPath("*/*/");
    }

}